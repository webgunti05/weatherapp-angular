import { Component, OnInit, Input } from '@angular/core';
import {City} from '../city';

@Component({
    selector: 'bgcomponent',
    template: `
        <div>
            <div *ngIf="city">
               <img src={{cityBg}} alt={{city}} width="100%" />
            </div>
        </div>
    `
})

export class BgComponent implements OnInit{
    
    @Input() city?: City;  
    cityBg: string = "";
    constructor(){}

    getCityBgs(){
        switch(this.city as any){
            case 'London' :
            this.cityBg = "../assets/London_bg.jpg";
            break;
            case "Manchester" :
            this.cityBg = "../assets/Manchester_bg.jpg";
            break;
            case "Liverpool" :
            this.cityBg = "../assets/Liverpool_bg.jpg";
            break;
            case "Glasgow" :
            this.cityBg = "../assets/Glasgow_bg.jpg";
            break;
            case "Birmingham" :
            this.cityBg = "../assets/Birmingham_bg.jpg";
            break;
            default :
            this.cityBg = "../assets/image-not-found.jpg";
        }
    }

    ngOnInit(){
        this.getCityBgs();
    }
}