import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UkCitiesWeatherApiService {
  apiURL: string = 'http://api.openweathermap.org/data/2.5/';
  AppID = "3d8b309701a13f65b660fa2c64cdc517"
  londonCity: string = "London";
  liverpoolCity: string = "Liverpool";
  birminghamCity: string = "Birmingham";
  manchesterCity: string = "Manchester";
  glasgowCity: string = "Glasgow";
  constructor(private httpClient: HttpClient) { 

  }

  public getLondonCityData(){
    return this.httpClient.get(`${this.apiURL}/weather?q=${this.londonCity}&appid=${this.AppID}`);
  }
  public getLiverpoolCityData(){
    return this.httpClient.get(`${this.apiURL}/weather?q=${this.liverpoolCity}&appid=${this.AppID}`);
  }
  public getBirminghamData(){
    return this.httpClient.get(`${this.apiURL}/weather?q=${this.birminghamCity}&appid=${this.AppID}`);
  }
  public getManchesterData(){
    return this.httpClient.get(`${this.apiURL}/weather?q=${this.manchesterCity}&appid=${this.AppID}`);
  }
  public getGlasgowData(){
    return this.httpClient.get(`${this.apiURL}/weather?q=${this.glasgowCity}&appid=${this.AppID}`);
  }
  public getDeailedCityWeather(id:any, count=5, appid = "3d8b309701a13f65b660fa2c64cdc517"){
    return this.httpClient.get(`${this.apiURL}forecast?id=${id}&cnt=${count}&appid=${appid}`)
  }
}
