import { TestBed } from '@angular/core/testing';

import { UkCitiesWeatherApiService } from './uk-cities-weather-api.service';

describe('UkCitiesWeatherApiService', () => {
  let service: UkCitiesWeatherApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UkCitiesWeatherApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
