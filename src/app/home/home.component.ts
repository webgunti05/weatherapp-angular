import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {UkCitiesWeatherApiService} from '../services/uk-cities-weather-api.service';

@Component({
    selector: 'home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css'],

})

export class HomeComponent implements OnInit {

     AllCitiesWeather : any = [];

    constructor(public ldsSerivce:UkCitiesWeatherApiService, public route:Router){
       
    }

    //fetching London city Weather
    getLondonWeather(){
        this.ldsSerivce.getLondonCityData().subscribe((response:any) => {
            this.AllCitiesWeather.push(response);
        });    
    }
    //fetching Liverpool city Weather
    getLiverPoolWeather(){
        this.ldsSerivce.getLiverpoolCityData().subscribe((response:any) => {
            this.AllCitiesWeather.push(response);
        });
    }
    //fetching Birmingham city Weather
    getBirminghamWeather(){
        this.ldsSerivce.getBirminghamData().subscribe((response:any) => {
            this.AllCitiesWeather.push(response);
        });
    }
    //fetching Manchester city Weather
    getManchesterWeather(){
        this.ldsSerivce.getManchesterData().subscribe((response:any) => {
            this.AllCitiesWeather.push(response);
        });
    }
     //fetching Glassgow city Weather
     getGlasgowWeather(){
        this.ldsSerivce.getGlasgowData().subscribe((response:any) => {
            this.AllCitiesWeather.push(response);
        });
    }

    openDetailsPage(id:any){
        let routePather = decodeURIComponent(`/weather/${id}`);
        this.route.navigate([routePather]);
    }

    ngOnInit(){
        this.getLondonWeather();
        this.getLiverPoolWeather();
        this.getBirminghamWeather();
        this.getManchesterWeather();
        this.getGlasgowWeather();
    }
    
}