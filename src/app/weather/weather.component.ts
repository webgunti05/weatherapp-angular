import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {UkCitiesWeatherApiService} from '../services/uk-cities-weather-api.service';

@Component({
    selector: 'weather',
    templateUrl: './weather.component.html',
    styleUrls : ['./weather.component.css']
})

export class WeatherComponent implements OnInit{
    cityId : any;
    weatherDetailData: any
    constructor(public ldsSerivce:UkCitiesWeatherApiService, public routeparam: ActivatedRoute){};


    ngOnInit(){
       this.routeparam.paramMap.subscribe(params => {
         this.cityId = params.get('id');
       });

       this.ldsSerivce.getDeailedCityWeather(this.cityId).subscribe((response:any) => {
        this.weatherDetailData = response;
    })
        
    }
}